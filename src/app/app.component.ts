import { Component } from "@angular/core";
import { NgbModalConfig, NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  tags = "";
  title = "tagCrud";
  Numbers = [];
  tag = [];
  editText: string;
  //tag = new FormControl("");

  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    // customize default values of modals used by this component tree
    config.backdrop = "static";
    config.keyboard = false;
  }

  ngOnInit() {
    var retrievedData = localStorage.getItem("Tags");
    console.log("RetriveData--:", JSON.parse(retrievedData));
    this.tag = JSON.parse(retrievedData);
    this.Numbers = JSON.parse(retrievedData);
    console.log("Tag---", this.tag);
  }

  //add tag in localStorage
  addTag(tags: any) {
    this.tags = "";
    var arrTag = tags.split(/[\s,;\t\n]+/);
    var NN = [];
    if (localStorage.getItem("Tags")) {
      var dd = JSON.parse(localStorage.getItem("Tags"));
      dd.forEach(d1 => {
        NN.push(d1);
      });
    }
    //console.log("arrTag", arrTag);
    arrTag.forEach(element => {
      if (element.match(/^-{0,1}\d+$/)) {
        //console.log("Number---", element);
        if (element >= 0) {
          // console.log("Positive----", element);
          NN.push(element);
        } else {
          // console.log("Negative---", element);
          NN.push(element);
        }
      } else if (element.match(/^\d+\.\d+$/)) {
        //console.log("Float---", element);
      } else {
        // console.log("String---", element);
      }
    });
    this.tag = NN;
    localStorage.setItem("Tags", JSON.stringify(this.tag));
  }

  //remove tag from localStorage
  deleteTag(Dtag: any) {
    this.tag.splice(Dtag, 1);
    localStorage.setItem("Tags", JSON.stringify(this.tag));
  }

  //Open Modal window when click on Edit tags button
  open(content) {
    this.modalService.open(content);
    var data = JSON.parse(localStorage.getItem("Tags"));
    this.editText = data.join(" , ");
    // console.log("Edit-Text---", this.editText);
  }

  //Edit tags
  onSave(newTags: any) {
    var NN = [];
    localStorage.removeItem("Tags");
    var arrTag = newTags.split(/[\s,;\t\n]+/);
    arrTag.forEach(element => {
      if (element.match(/^-{0,1}\d+$/)) {
        //console.log("Number---", element);
        if (element >= 0) {
          // console.log("Positive----", element);
          NN.push(element);
        } else {
          // console.log("Negative---", element);
          NN.push(element);
        }
      } else if (element.match(/^\d+\.\d+$/)) {
        //console.log("Float---", element);
      } else {
        // console.log("String---", element);
      }
    });
    this.tag = NN;
    localStorage.setItem("Tags", JSON.stringify(this.tag));
  }
}
